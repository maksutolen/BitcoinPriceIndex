package com.choco.bitcoin.ui.main;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.choco.bitcoin.R;
import com.choco.bitcoin.data.models.currency.CurrentPrice;
import com.choco.bitcoin.mvp.main.MainPresenter;
import com.choco.bitcoin.mvp.main.MainView;
import com.choco.bitcoin.ui.base.BaseActivity;
import com.choco.bitcoin.ui.base.view_pager.ViewPagerAdapter;
import com.choco.bitcoin.ui.main.currency.CurrencyFragment;
import com.choco.bitcoin.ui.main.transactions.TransactionsFragment;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;

import static com.choco.bitcoin.utils.StringUtils.EMPTY_STR;

public class MainActivity extends BaseActivity implements MainView {
    @InjectPresenter
    MainPresenter mvpPresenter;
    @BindColor(R.color.colorPrimary) int colorPrimary;
    @BindView(R.id.pager) ViewPager viewPager;
    @BindView(R.id.tabs) CommonTabLayout tabLayout;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureViewPager();
    }

    private void configureViewPager() {
        ArrayList<CustomTabEntity> tabEntities = new ArrayList<>(2);
        tabEntities.add(new TabEntity(EMPTY_STR, R.drawable.ic_tab_currency_active,
                R.drawable.ic_tab_currency_default));
        tabEntities.add(new TabEntity(EMPTY_STR, R.drawable.ic_tab_history_active,
                R.drawable.ic_tab_history_default));

        tabLayout.setTabData(tabEntities);
        tabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {
            }
        });

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(CurrencyFragment.newInstance());
        pagerAdapter.addFragment(TransactionsFragment.newInstance());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.setCurrentTab(position);
            }
        });
        viewPager.setCurrentItem(0);
    }

    static class TabEntity implements CustomTabEntity {
        public String title;
        private int selectedIcon;
        private int unSelectedIcon;

        private TabEntity(String title, int selectedIcon, int unSelectedIcon) {
            this.title = title;
            this.selectedIcon = selectedIcon;
            this.unSelectedIcon = unSelectedIcon;
        }

        @Override
        public String getTabTitle() {
            return title;
        }

        @Override
        public int getTabSelectedIcon() {
            return selectedIcon;
        }

        @Override
        public int getTabUnselectedIcon() {
            return unSelectedIcon;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // MainView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setUser(CurrentPrice response) {
//        GlideUtils.showAvatar(this, ivAvatar, response.thumb, R.drawable.avatar_transparent);
//        tvFullName.setText(StringUtils.okTxt(response.full_name, tvFullName));
    }
}