package com.choco.bitcoin.ui.main.transactions;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.choco.bitcoin.R;
import com.choco.bitcoin.data.models.transactions.Transaction;
import com.choco.bitcoin.mvp.main.transactions.TransactionsPresenter;
import com.choco.bitcoin.mvp.main.transactions.TransactionsView;
import com.choco.bitcoin.ui.base.BaseFragment;
import com.choco.bitcoin.utils.recycler_view.SwipeRefreshUtils;

import java.util.List;

import butterknife.BindView;

public class TransactionsFragment extends BaseFragment implements TransactionsView, TransactionsAdapter.Callback {
    @InjectPresenter
    TransactionsPresenter mvpPresenter;

    private TransactionsAdapter adapter;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipe_refresh_layout;
    @BindView(R.id.recycler_view) RecyclerView recycler_view;

    public static TransactionsFragment newInstance() {
        return new TransactionsFragment();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_transactions;
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState, View view) {
        configureRecyclerView();
        mvpPresenter.getTransactions(false);
    }

    @Override
    public void onDestroy() {
        recycler_view.clearOnScrollListeners();
        recycler_view.setAdapter(null);
        swipe_refresh_layout.setOnRefreshListener(null);
        super.onDestroy();
    }

    private void configureRecyclerView() {
        adapter = new TransactionsAdapter(getContext());
        adapter.setCallback(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setHasFixedSize(true);
        recycler_view.setAdapter(adapter);
        SwipeRefreshUtils.setColorSchemeColors(getContext(), swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(() -> mvpPresenter.getTransactions(true));
    }

    ///////////////////////////////////////////////////////////////////////////
    // TransactionsView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setTransactions(List<Transaction> list) {
        adapter.clearItems();
        recycler_view.post(() -> adapter.addList(list));
    }

    @Override
    public void showRefreshingIndicator(boolean show) {
        swipe_refresh_layout.setRefreshing(show);
    }


    ///////////////////////////////////////////////////////////////////////////
    // TransactionsAdapter.Callback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onItemClick(String id) {

    }
}