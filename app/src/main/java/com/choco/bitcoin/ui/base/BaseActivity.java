package com.choco.bitcoin.ui.base;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.choco.bitcoin.R;
import com.choco.bitcoin.mvp.base.BaseMvpView;
import com.choco.bitcoin.utils.SnackbarUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.choco.bitcoin.utils.ToastUtils.showToast;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseMvpView {
    @BindView(R.id.fl_container) protected FrameLayout fl_container;
    @BindView(R.id.pb_indicator) protected ViewGroup pb_indicator;

    protected abstract int getLayoutResourceId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutResourceId() != -1) setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ///////////////////////////////////////////////////////////////////////////
    // BaseMvpView implementation                                           ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showRequestError(String errorMessage) {
        SnackbarUtils.showSnackbar(fl_container, errorMessage);
    }

    @Override
    public void showRequestError(@StringRes int errorRes) {
        SnackbarUtils.showSnackbar(fl_container, getString(errorRes));
    }

    @Override
    public void showRequestSuccess(int message) {
        showToast(this, getString(message));
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showEmptyData() {
        SnackbarUtils.showSnackbar(fl_container, getString(R.string.no_data));
    }
}
