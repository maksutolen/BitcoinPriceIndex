package com.choco.bitcoin.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.choco.bitcoin.R;
import com.choco.bitcoin.mvp.base.BaseMvpView;
import com.choco.bitcoin.utils.SnackbarUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.choco.bitcoin.utils.ToastUtils.showToast;

public abstract class BaseFragment extends MvpAppCompatFragment implements BaseMvpView {
    @BindView(R.id.fl_container) protected FrameLayout fl_container;
    @BindView(R.id.pb_indicator) protected ViewGroup pb_indicator;

    FragmentNavigation mFragmentNavigation;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected abstract int getLayoutResourceId();

    protected abstract void onViewCreated(Bundle savedInstanceState, View view);

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResourceId(), container, false);
        ButterKnife.bind(this, view);
        onViewCreated(savedInstanceState, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentNavigation) {
            mFragmentNavigation = (FragmentNavigation) context;
        }
    }

    public interface FragmentNavigation {
        public void pushFragment(Fragment fragment);
    }

    ///////////////////////////////////////////////////////////////////////////
    // BaseMvpView implementation                                           ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showRequestSuccess(int message) {
        showToast(getContext(), getString(message));
    }

    @Override
    public void showRequestError(String errorMessage) {
        SnackbarUtils.showSnackbar(fl_container, errorMessage);
    }

    @Override
    public void showRequestError(@StringRes int errorRes) {
        SnackbarUtils.showSnackbar(fl_container, getString(errorRes));
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showEmptyData() {
        SnackbarUtils.showSnackbar(fl_container, getString(R.string.no_data));
    }
}
