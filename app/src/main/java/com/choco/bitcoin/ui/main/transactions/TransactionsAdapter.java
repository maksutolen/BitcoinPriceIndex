package com.choco.bitcoin.ui.main.transactions;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.choco.bitcoin.R;
import com.choco.bitcoin.data.models.transactions.Transaction;
import com.choco.bitcoin.ui.base.RecyclerViewBaseAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.choco.bitcoin.data.models.transactions.Transaction.TYPE_BUY;
import static com.choco.bitcoin.data.models.transactions.Transaction.TYPE_SELL;

class TransactionsAdapter extends RecyclerViewBaseAdapter {
    private static final int LAYOUT_ID = R.layout.adapter_transactions;

    private List<Transaction> data;
    private Callback callback;
    private Context context;

    TransactionsAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    void addList(List<Transaction> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    void clearItems() {
        data.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case LAYOUT_ID:
                return new ItemViewHolder(inflate(parent, LAYOUT_ID));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return LAYOUT_ID;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LAYOUT_ID:
                ((ItemViewHolder) holder).bind(data.get(position), position);
                break;
        }
    }

    class ItemViewHolder extends MainViewHolder {
        @BindView(R.id.tv_amount) TextView tv_amount;
        @BindView(R.id.tv_transaction_type) TextView tv_transaction_type;
        @BindView(R.id.tv_date) TextView tv_date;
        @BindColor(R.color.green) int buyColor;
        @BindColor(R.color.main_btn_default) int sellColor;

        Transaction bindedObject;
        int bindedPosition;

        ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Transaction transaction, int position) {
            bindedObject = transaction;
            bindedPosition = position;
            if (transaction == null) {
                return;
            }
            tv_amount.setText(context.getString(R.string.label_amount, transaction.getAmount()));
            tv_date.setText(transaction.getDateTimeStr());
            if (transaction.getType() == TYPE_BUY) {
                tv_transaction_type.setText(context.getString(R.string.type_buy));
                tv_transaction_type.setTextColor(buyColor);
            } else if (transaction.getType() == TYPE_SELL) {
                tv_transaction_type.setText(context.getString(R.string.type_sell));
                tv_transaction_type.setTextColor(sellColor);
            }
        }
    }

    public interface Callback {
        void onItemClick(String id);
    }
}