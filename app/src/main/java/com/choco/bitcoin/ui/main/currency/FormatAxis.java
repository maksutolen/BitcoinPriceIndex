package com.choco.bitcoin.ui.main.currency;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import static com.choco.bitcoin.utils.StringUtils.EMPTY_STR;


public class FormatAxis implements IAxisValueFormatter {
    private String[] titles;

    FormatAxis(String[] titles) {
        this.titles = titles;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        int range = (int) axis.mAxisRange;
        int iValue = (int) value;
        if (iValue < range) {
            return titles[iValue];
        } else {
            return EMPTY_STR;
        }
    }
}
