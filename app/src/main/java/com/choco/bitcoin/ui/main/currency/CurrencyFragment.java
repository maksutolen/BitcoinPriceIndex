package com.choco.bitcoin.ui.main.currency;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.choco.bitcoin.R;
import com.choco.bitcoin.data.models.currency.Currency;
import com.choco.bitcoin.mvp.main.currency.CurrencyPresenter;
import com.choco.bitcoin.mvp.main.currency.CurrencyView;
import com.choco.bitcoin.ui.base.BaseFragment;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;

import static com.choco.bitcoin.utils.StringUtils.EMPTY_STR;

public class CurrencyFragment extends BaseFragment implements CurrencyView {
    @InjectPresenter
    CurrencyPresenter mvpPresenter;
    @BindView(R.id.tv_rate) TextView tv_rate;
    @BindView(R.id.sp_currency) Spinner sp_currency;
    @BindView(R.id.sp_date) Spinner sp_date;
    @BindView(R.id.chart_currency) BarChart chart_currency;
    @BindString(R.string.week_label) String week_label;

    public static CurrencyFragment newInstance() {
        return new CurrencyFragment();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_currency;
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState, View view) {
        mvpPresenter.setWeekLabel(week_label);
        mvpPresenter.setListOfMonth(getResources().getStringArray(R.array.list_of_month));
        configureSpinner(sp_currency, R.array.currencies, true);
        configureSpinner(sp_date, R.array.date_filters, false);
        configureChart();
    }

    void configureSpinner(Spinner spinner, int array, boolean isCurrency) {
        if (getContext() == null) return;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.adapter_spinner,
                getResources().getStringArray(array));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (isCurrency) {
                    mvpPresenter.selectCurrency(position);
                }
                mvpPresenter.getHistoricalCurrency(sp_currency.getSelectedItemPosition(),
                        sp_date.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        spinner.setAdapter(adapter);

    }

    private void configureChart() {
        chart_currency.setDrawBarShadow(false);
        chart_currency.setDrawValueAboveBar(true);
        chart_currency.getDescription().setEnabled(false);
        chart_currency.setMaxVisibleValueCount(60);
        chart_currency.setPinchZoom(false);
        chart_currency.setDrawGridBackground(false);
    }

    private void setData(ArrayList<BarEntry> chartValues, String[] titles) {
        BarDataSet barDataSet = new BarDataSet(chartValues, EMPTY_STR);
        barDataSet.setDrawIcons(false);
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(barDataSet);

        // Horizontal titles
        IAxisValueFormatter xAxisFormatter = new FormatAxis(titles);
        XAxis xAxis = chart_currency.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setDrawGridLines(true);
        xAxis.setValueFormatter(xAxisFormatter);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.9f);
        chart_currency.setData(data);
        chart_currency.invalidate();
        chart_currency.notifyDataSetChanged();
    }

    ///////////////////////////////////////////////////////////////////////////
    // CurrencyView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showCurrency(Currency currency) {
        tv_rate.setText(String.valueOf(currency.getRateValue() + " " + currency.getCode()));
    }

    @Override
    public void setHistoricalCurrencies(ArrayList<BarEntry> chartValues, String[] titles) {
        setData(chartValues, titles);
    }
}