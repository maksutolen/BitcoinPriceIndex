package com.choco.bitcoin.config;

public class AppConfig {
    public static final String SCHEME = "https://";

    public static final String COIN_DESK_DOMAIN = "api.coindesk.com/";
    public static final String COIN_DESK_BASE_URL = SCHEME + COIN_DESK_DOMAIN + "v1/bpi/";

    public static final String BITSTAMP_DOMAIN = "www.bitstamp.net/";
    public static final String BITSTAMP_BASE_URL = SCHEME + BITSTAMP_DOMAIN + "api/";
}