package com.choco.bitcoin.config;

import com.choco.bitcoin.BuildConfig;

public class DebugConfig {
    public static final boolean DEV_BUILD = BuildConfig.DEBUG;
}