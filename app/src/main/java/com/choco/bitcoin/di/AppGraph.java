package com.choco.bitcoin.di;

import com.choco.bitcoin.mvp.main.currency.CurrencyPresenter;
import com.choco.bitcoin.mvp.main.transactions.TransactionsPresenter;

public interface AppGraph {
    void inject(CurrencyPresenter presenter);

    void inject(TransactionsPresenter presenter);
}