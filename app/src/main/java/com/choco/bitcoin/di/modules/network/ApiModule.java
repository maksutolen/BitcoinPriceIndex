package com.choco.bitcoin.di.modules.network;

import com.choco.bitcoin.data.network.api.BitstampAPI;
import com.choco.bitcoin.data.network.api.CoinDeskAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static com.choco.bitcoin.config.AppConfig.BITSTAMP_BASE_URL;
import static com.choco.bitcoin.config.AppConfig.COIN_DESK_BASE_URL;

@Module(includes = {RetrofitModule.class})
public class ApiModule {
    @Provides
    @Singleton
    CoinDeskAPI provideCoinDeskAPI(Retrofit.Builder builder, OkHttpClient okHttpClient) {
        return builder
                .baseUrl(COIN_DESK_BASE_URL)
                .client(okHttpClient)
                .build()
                .create(CoinDeskAPI.class);
    }

    @Provides
    @Singleton
    BitstampAPI provideBitsampAPI(Retrofit.Builder builder, OkHttpClient okHttpClient) {
        return builder
                .baseUrl(BITSTAMP_BASE_URL)
                .client(okHttpClient)
                .build()
                .create(BitstampAPI.class);
    }
}