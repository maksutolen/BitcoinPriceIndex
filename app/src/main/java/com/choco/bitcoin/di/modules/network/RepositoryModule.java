package com.choco.bitcoin.di.modules.network;

import com.choco.bitcoin.data.network.api.BitstampAPI;
import com.choco.bitcoin.data.network.api.CoinDeskAPI;
import com.choco.bitcoin.data.repository.bitstamp.BitstampRepository;
import com.choco.bitcoin.data.repository.coin_desk.CoinDeskRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class})
public class RepositoryModule {
	@Provides
	@Singleton
	CoinDeskRepository provideCoinDeskRepository(CoinDeskAPI api) {
		return new CoinDeskRepository(api);
	}

	@Provides
	@Singleton
	BitstampRepository provideBitstampRepository(BitstampAPI api) {
		return new BitstampRepository(api);
	}
}