package com.choco.bitcoin.mvp.main.transactions;

import com.arellomobile.mvp.InjectViewState;
import com.choco.bitcoin.ChocoBitcoinApp;
import com.choco.bitcoin.data.models.transactions.Transaction;
import com.choco.bitcoin.data.repository.bitstamp.BitstampRepository;
import com.choco.bitcoin.mvp.base.BaseMvpPresenter;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;

import static com.choco.bitcoin.utils.rx.RxUtils.isActive;

@InjectViewState
public class TransactionsPresenter extends BaseMvpPresenter<TransactionsView> {
    @Inject
    BitstampRepository repository;

    private Subscription transactionsSubscription;

    public TransactionsPresenter() {
        ChocoBitcoinApp.getAppGraph().inject(this);
        getTransactions(false);
    }

    public void getTransactions(boolean is_refresh) {
        if (isActive(transactionsSubscription)) {
            return;
        }
        transactionsSubscription = repository.getTransactions()
                .doOnSubscribe(() -> showLoading(true, is_refresh))
                .subscribe(new Subscriber<List<Transaction>>() {
                    @Override
                    public void onCompleted() {
                        showLoading(false, is_refresh);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showLoading(false, is_refresh);
                        handleResponseError(e);
                    }

                    @Override
                    public void onNext(List<Transaction> transactions) {
                        if (transactions != null && transactions.size() > 0) {
                            getViewState().setTransactions(transactions);
                        }
                    }
                });
    }

    private void showLoading(boolean show, boolean is_refresh) {
        if (!is_refresh) {
            getViewState().showLoadingIndicator(show);
        } else {
            getViewState().showRefreshingIndicator(show);
        }
    }
}