package com.choco.bitcoin.mvp.main;

import com.choco.bitcoin.data.models.currency.CurrentPrice;
import com.choco.bitcoin.mvp.base.BaseMvpView;

public interface MainView extends BaseMvpView {
    void setUser(CurrentPrice response);
}