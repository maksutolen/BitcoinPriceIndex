package com.choco.bitcoin.mvp.main.currency;

import com.arellomobile.mvp.InjectViewState;
import com.choco.bitcoin.ChocoBitcoinApp;
import com.choco.bitcoin.data.models.currency.CurrentPrice;
import com.choco.bitcoin.data.models.currency.HistoricalCurrency;
import com.choco.bitcoin.data.repository.coin_desk.CoinDeskRepository;
import com.choco.bitcoin.mvp.base.BaseMvpPresenter;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;

import static com.choco.bitcoin.utils.DateUtils.getDayBeforeMonth;
import static com.choco.bitcoin.utils.DateUtils.getDayBeforeWeek;
import static com.choco.bitcoin.utils.DateUtils.getDayBeforeYear;
import static com.choco.bitcoin.utils.DateUtils.getTodayDate;

@InjectViewState
public class CurrencyPresenter extends BaseMvpPresenter<CurrencyView> {
    @Inject
    CoinDeskRepository repository;

    private CurrentPrice mCurrentPrice;
    private CurrentPrice mCurrentPriceKZT;
    private String[] data = {"usd", "eur", "kzt"};
    private String[] listOfMonth;
    private String weekLabel;

    public CurrencyPresenter() {
        ChocoBitcoinApp.getAppGraph().inject(this);
        getCurrency();
        getCurrencyKZT();
    }

    public void setWeekLabel(String weekLabel) {
        this.weekLabel = weekLabel;
    }

    public void setListOfMonth(String[] listOfMonth) {
        this.listOfMonth = listOfMonth;
    }

    private void getCurrency() {
        Subscription subscription = repository.getCurrency()
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<CurrentPrice>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(e);
                    }

                    @Override
                    public void onNext(CurrentPrice currentPrice) {
                        if (currentPrice != null) {
                            mCurrentPrice = currentPrice;
                            selectCurrency(0);
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    private void getCurrencyKZT() {
        Subscription subscription = repository.getCurrencyKZT()
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<CurrentPrice>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(e);
                    }

                    @Override
                    public void onNext(CurrentPrice currentPrice) {
                        if (currentPrice != null) {
                            mCurrentPriceKZT = currentPrice;
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void getHistoricalCurrency(int currencyPosition, int dateFilterPosition) {
        String startDate;
        String endDate = getTodayDate();
        if (dateFilterPosition == 0) {
            startDate = getDayBeforeWeek();
        } else if (dateFilterPosition == 1) {
            startDate = getDayBeforeMonth();
        } else {
            startDate = getDayBeforeYear();
        }
        Subscription subscription = repository.getHistoricalCurrency(startDate, endDate, data[currencyPosition])
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<HistoricalCurrency>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(e);
                    }

                    @Override
                    public void onNext(HistoricalCurrency historical) {
                        if (historical != null && historical.bpi != null) {
                            ArrayList<String> keys = new ArrayList<>();
                            ArrayList<BarEntry> chartValues = new ArrayList<>();
                            if (dateFilterPosition == 0) {
                                // ****** Week chart ******
                                int horizontalRange = 0;
                                for (String key : historical.bpi.keySet()) {
                                    // substring date value for showing day and month (DD-MM)
                                    keys.add(key.substring(5, key.length()));
                                    chartValues.add(new BarEntry(horizontalRange, historical.bpi.get(key)));
                                    horizontalRange += 1;
                                }
                            } else if (dateFilterPosition == 1) {
                                // ****** Month chart ******
                                int i = 1, weekCount = 0;
                                float total = 0;
                                for (String key : historical.bpi.keySet()) {
                                    total = total + historical.bpi.get(key);
                                    if (i % 7 == 0) {
                                        // Take each week and calculate middle value
                                        keys.add((weekCount + 1) + "" + weekLabel);
                                        chartValues.add(new BarEntry(weekCount, (total / 7)));
                                        weekCount += 1;
                                        total = 0;
                                    }
                                    i += 1;
                                }
                            } else {
                                // ****** Year chart ******
                                int i = 1, monthCount = 0;
                                float total = 0;
                                for (String key : historical.bpi.keySet()) {
                                    total = total + historical.bpi.get(key);
                                    if (i % 30 == 0) {
                                        // Take each month and calculate middle value
                                        // substring date value for showing month (DD)
                                        int monthIndex = Integer.parseInt(key.substring(5, key.length() - 3));
                                        keys.add(listOfMonth[monthIndex - 1]);
                                        chartValues.add(new BarEntry(monthCount, (total / 30)));
                                        monthCount += 1;
                                        total = 0;
                                    }
                                    i += 1;
                                }
                            }
                            getViewState().setHistoricalCurrencies(chartValues, keys.toArray(new String[keys.size()]));
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void selectCurrency(int position) {
        switch (position) {
            case 0:
                if (mCurrentPrice != null && mCurrentPrice.bpi != null && mCurrentPrice.bpi.USD != null) {
                    getViewState().showCurrency(mCurrentPrice.bpi.USD);
                }
                break;
            case 1:
                if (mCurrentPrice != null && mCurrentPrice.bpi != null && mCurrentPrice.bpi.EUR != null) {
                    getViewState().showCurrency(mCurrentPrice.bpi.EUR);
                }
                break;
            default:
                if (mCurrentPriceKZT != null && mCurrentPriceKZT.bpi != null && mCurrentPriceKZT.bpi.KZT != null) {
                    getViewState().showCurrency(mCurrentPriceKZT.bpi.KZT);
                }
        }
    }
}