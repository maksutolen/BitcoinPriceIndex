package com.choco.bitcoin.mvp.main;

import com.arellomobile.mvp.InjectViewState;
import com.choco.bitcoin.mvp.base.BaseMvpPresenter;

@InjectViewState
public class MainPresenter extends BaseMvpPresenter<MainView> {

}