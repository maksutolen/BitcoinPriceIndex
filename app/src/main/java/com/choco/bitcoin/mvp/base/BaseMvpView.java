package com.choco.bitcoin.mvp.base;


import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;

public interface BaseMvpView extends MvpView {

    void showRequestError(String errorMessage);

    void showRequestError(@StringRes int errorRes);

    void showRequestSuccess(@StringRes int errorRes);

    void showLoadingIndicator(boolean show);

    void showEmptyData();
}
