package com.choco.bitcoin.mvp.main.transactions;

import com.choco.bitcoin.data.models.transactions.Transaction;
import com.choco.bitcoin.mvp.base.BaseMvpView;

import java.util.List;

public interface TransactionsView extends BaseMvpView {
    void showRefreshingIndicator(boolean show);

    void setTransactions(List<Transaction> list);
}