package com.choco.bitcoin.mvp.base;

import com.arellomobile.mvp.MvpPresenter;
import com.choco.bitcoin.R;
import com.choco.bitcoin.data.network.error.RetrofitErrorHandler;
import com.choco.bitcoin.data.network.exceptions.APIException;
import com.choco.bitcoin.data.network.exceptions.ConnectionTimeOutException;
import com.choco.bitcoin.data.network.exceptions.UnknownException;

import rx.subscriptions.CompositeSubscription;

import static com.choco.bitcoin.config.DebugConfig.DEV_BUILD;
import static com.choco.bitcoin.utils.StringUtils.replaceNull;

public abstract class BaseMvpPresenter<T extends BaseMvpView> extends MvpPresenter<T> {
    protected CompositeSubscription compositeSubscription = new CompositeSubscription();

    protected void handleResponseError(Throwable e) {
        if (DEV_BUILD) {
            e.printStackTrace();
        }
        try {
            RetrofitErrorHandler.handleException(e);
        } catch (APIException e1) {
            getViewState().showRequestError(replaceNull(e1.getErrorDescr()));
        } catch (UnknownException e1) {
            getViewState().showRequestError(R.string.error_occurred);
        } catch (ConnectionTimeOutException e1) {
            getViewState().showRequestError(R.string.connection_error);
        }
    }

    public void onDestroyView() {
        unSubscribe();
    }

    private void unSubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}
