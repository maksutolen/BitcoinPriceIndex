package com.choco.bitcoin.mvp.main.currency;

import com.choco.bitcoin.data.models.currency.Currency;
import com.choco.bitcoin.mvp.base.BaseMvpView;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Map;

public interface CurrencyView extends BaseMvpView {
    void showCurrency(Currency usd);

    void setHistoricalCurrencies(ArrayList<BarEntry> chartValues, String[] titles);
}