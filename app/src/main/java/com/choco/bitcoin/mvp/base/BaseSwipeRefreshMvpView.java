package com.choco.bitcoin.mvp.base;

public interface BaseSwipeRefreshMvpView extends BaseMvpView {

    void showRefreshLoading(boolean show);
}
