package com.choco.bitcoin;

import com.choco.bitcoin.di.AppGraph;
import com.choco.bitcoin.di.modules.network.RepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {RepositoryModule.class})
public interface AppComponent extends AppGraph {
    final class Initializer {
        static AppGraph init(final ChocoBitcoinApp app) {
            return DaggerAppComponent.builder()
                    .build();
        }
    }
}