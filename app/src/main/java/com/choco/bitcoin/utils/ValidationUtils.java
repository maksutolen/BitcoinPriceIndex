package com.choco.bitcoin.utils;

import android.util.Patterns;

import java.util.regex.Pattern;

public class ValidationUtils {
    private static final String USERNAME_PATTERN = "^[a-z0-9_-]{2,40}$";

    public static boolean isValidUsername(String username) {
        return Pattern.compile(USERNAME_PATTERN).matcher(username).matches();
    }

    public static boolean isValidEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
