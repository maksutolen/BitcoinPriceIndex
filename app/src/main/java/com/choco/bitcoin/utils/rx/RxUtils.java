package com.choco.bitcoin.utils.rx;

import rx.Subscription;

public class RxUtils {
    public static boolean isActive(Subscription s) {
        return s != null && !s.isUnsubscribed();
    }

}
