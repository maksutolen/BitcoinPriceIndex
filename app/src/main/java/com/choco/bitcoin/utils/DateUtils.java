package com.choco.bitcoin.utils;

import android.support.annotation.Nullable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.choco.bitcoin.utils.StringUtils.EMPTY_STR;

public class DateUtils {
    public static final DateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    /**
     * parse @{@link String} yyyymmdd to @{@link Date} format
     *
     * @param date
     * @return
     */
    @Nullable
    public static Date parseDate(String date) {
        try {
            DateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
            return df.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static String dateFormat(Date date) {
        if (date == null) return EMPTY_STR;
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
    }

    public static String getDayBeforeWeek() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -7);
        return YYYY_MM_DD.format(cal.getTime());
    }

    public static String getDayBeforeMonth() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        return YYYY_MM_DD.format(cal.getTime());
    }

    public static String getDayBeforeYear() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);
        return YYYY_MM_DD.format(cal.getTime());
    }

    public static String getTodayDate() {
        Calendar cal = Calendar.getInstance();
        return YYYY_MM_DD.format(cal.getTime());
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static String getDateString(Date date) {
        return new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(date);
    }

    public static String getDateMMMString(Date date) {
        return new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(date);
    }

    public static String getDateYYYYString(Date date) {
        return new SimpleDateFormat("yyyy", Locale.getDefault()).format(date);
    }

    public static String getMMMString(Date date) {
        return new SimpleDateFormat("MMMMM yyyy", Locale.getDefault()).format(date);
    }

    public static String getMonthString(Date date) {
        return new SimpleDateFormat("MMMM", Locale.getDefault()).format(date);
    }

    public static String getDateTimeString(long timestamp) {
        Date date = new Date(timestamp * 1000);
        return new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault()).format(date);
    }

    public static String getTimeAsString(Date date) {
        return new SimpleDateFormat("HH:mm", Locale.US).format(date);
    }

    public static boolean isToday(Date d1) {
        return isTheSameDay(d1, new Date());
    }

    public static boolean isNextDay(Date d1, Date d2) {
        return isTheSameDay(d1, addDays(d2, 1));
    }

    public static boolean isPrevDay(Date d1, Date d2) {
        return isTheSameDay(d1, addDays(d2, -1));
    }


    public static boolean isNextDay(Date d1) {
        return isNextDay(d1, new Date());
    }

    public static boolean isPrevDay(Date d1) {
        return isPrevDay(d1, new Date());
    }

    public static boolean isTheSameDay(Date d1, Date d2) {
        if (d1 == null || d2 == null) {
            return false;
        }
        Calendar c1 = toCalendar(d1);
        Calendar c2 = toCalendar(d2);
        return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) == c2.get(Calendar.DATE);
    }

    public static Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static String convertMillisToTimer(long seconds) {
        Date date = new Date(seconds * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
        return sdf.format(date);
    }
}
