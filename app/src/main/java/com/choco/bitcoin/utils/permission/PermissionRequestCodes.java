package com.choco.bitcoin.utils.permission;

import android.Manifest;

public class PermissionRequestCodes {
    public static final int STORAGE = 3;
    public static final int LOCATION = 99;

    public static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    public static String PERM_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static String PERM_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
}