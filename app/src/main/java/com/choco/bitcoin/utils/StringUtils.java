package com.choco.bitcoin.utils;

import android.view.View;
import android.widget.TextView;

public class StringUtils {
    public static final String EMPTY_STR = "";

    public static String trim(String s) {
        return s != null ? s.trim() : null;
    }

    public static int length(String s) {
        return s == null ? 0 : s.length();
    }

    public static String removeLastChar(String str) {
        if (length(str) > 0) {
            return str.substring(0, str.length() - 1);
        } else {
            return StringUtils.EMPTY_STR;
        }
    }

    public static boolean isStringOk(String text) {
        return text != null && text.trim().length() > 0;
    }

    public static int getOkInt(Integer number) {
        return number != null ? number : 0;
    }

    public static double getOkDouble(Double number) {
        return number != null ? number : 0;
    }

    public static boolean getOkBoolean(Boolean bool) {
        return bool != null ? bool : false;
    }

    public static float getOkFloat(Float number) {
        return number != null ? number : 0;
    }

    public static long getOkLong(Long number) {
        return number != null ? number : 0;
    }

    public static String replaceNull(String text) {
        if (isStringOk(text)) {
            return text;
        } else {
            return EMPTY_STR;
        }
    }

    public static String okTxt(String text, TextView textView) {
        if (isStringOk(text)) {
            textView.setVisibility(View.VISIBLE);
            return text;
        } else {
            textView.setVisibility(View.GONE);
            return EMPTY_STR;
        }
    }
}