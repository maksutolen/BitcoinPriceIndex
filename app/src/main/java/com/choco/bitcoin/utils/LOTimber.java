package com.choco.bitcoin.utils;

import com.choco.bitcoin.BuildConfig;

import timber.log.Timber;

public class LOTimber {
    public static void d(String message) {
        if (BuildConfig.DEBUG) {
            Timber.d(message);
        }
    }
}