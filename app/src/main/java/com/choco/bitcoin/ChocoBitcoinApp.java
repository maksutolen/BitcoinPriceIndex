package com.choco.bitcoin;

import android.app.Application;

import com.choco.bitcoin.config.DebugConfig;
import com.choco.bitcoin.di.AppGraph;
import com.choco.bitcoin.utils.stetho.StethoCustomConfigBuilder;
import com.facebook.stetho.Stetho;

import timber.log.Timber;

public class ChocoBitcoinApp extends Application {
    private static AppGraph appGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        appGraph = AppComponent.Initializer.init(this);
        setupTimber();
        if (DebugConfig.DEV_BUILD) {
            setupStetho();
        }
    }

    public static AppGraph getAppGraph() {
        return appGraph;
    }

    private void setupTimber() {
        Timber.plant(new Timber.DebugTree());
    }

    private void setupStetho() {
        Stetho.Initializer initializer = new StethoCustomConfigBuilder(this)
                .viewHierarchyInspectorEnabled(false)
                .build();
        Stetho.initialize(initializer);
    }
}