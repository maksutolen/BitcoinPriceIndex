package com.choco.bitcoin.data.models.currency;

import static com.choco.bitcoin.utils.StringUtils.getOkFloat;
import static com.choco.bitcoin.utils.StringUtils.replaceNull;

public class Currency {
    public String code;
    public String symbol;
    public String rate;
    public String description;
    public Float rate_float;

    public Currency(String code, Float rate_float) {
        this.code = code;
        this.rate_float = rate_float;
    }

    public String getCode() {
        return replaceNull(code);
    }

    public String getSymbol() {
        return replaceNull(symbol);
    }

    public String getRate() {
        return replaceNull(rate);
    }

    public String getDescription() {
        return replaceNull(description);
    }

    public float getRateValue() {
        return getOkFloat(rate_float);
    }
}
