package com.choco.bitcoin.data.network.api;

import com.choco.bitcoin.data.models.transactions.Transaction;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface BitstampAPI {
    @GET("transactions/")
    Observable<List<Transaction>> getTransactions();
}