package com.choco.bitcoin.data.repository.coin_desk;

import com.choco.bitcoin.data.models.currency.CurrentPrice;
import com.choco.bitcoin.data.models.currency.HistoricalCurrency;
import com.choco.bitcoin.data.network.api.CoinDeskAPI;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CoinDeskRepository {
    private CoinDeskAPI coinDeskApi;
    public CoinDeskRepository(CoinDeskAPI coinDeskApi) {
        this.coinDeskApi = coinDeskApi;
    }

    public Observable<CurrentPrice> getCurrency() {
        return coinDeskApi
                .getCurrency()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<CurrentPrice> getCurrencyKZT() {
        return coinDeskApi
                .getCurrencyKZT()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<HistoricalCurrency> getHistoricalCurrency(String startDate, String endDate, String currency) {
        return coinDeskApi
                .getHistoricalCurrency(startDate, endDate, currency)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}