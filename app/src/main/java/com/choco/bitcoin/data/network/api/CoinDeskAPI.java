package com.choco.bitcoin.data.network.api;

import com.choco.bitcoin.data.models.currency.CurrentPrice;
import com.choco.bitcoin.data.models.currency.HistoricalCurrency;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface CoinDeskAPI {
    @GET("currentprice.json")
    Observable<CurrentPrice> getCurrency();

    @GET("currentprice/kzt.json")
    Observable<CurrentPrice> getCurrencyKZT();

    @GET("historical/currency.json")
    Observable<HistoricalCurrency> getHistoricalCurrency(@Query("start") String start,
                                                         @Query("end") String end,
                                                         @Query("currency") String currency);
}