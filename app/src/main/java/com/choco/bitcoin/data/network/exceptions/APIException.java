package com.choco.bitcoin.data.network.exceptions;

public class APIException extends Exception {

    private String errors;

    public APIException(String errors) {
        this.errors = errors;
    }

    public String getErrorDescr() {
        return errors;
    }
}