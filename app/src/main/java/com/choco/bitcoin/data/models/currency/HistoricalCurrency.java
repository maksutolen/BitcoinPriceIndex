package com.choco.bitcoin.data.models.currency;

import java.util.Map;

public class HistoricalCurrency {
    public Map<String, Float> bpi;
    public String disclaimer;
}
