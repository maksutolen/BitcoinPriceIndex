package com.choco.bitcoin.data.repository.bitstamp;

import com.choco.bitcoin.data.models.transactions.Transaction;
import com.choco.bitcoin.data.network.api.BitstampAPI;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BitstampRepository {
    private BitstampAPI bitstampAPI;

    public BitstampRepository(BitstampAPI bitstampAPI) {
        this.bitstampAPI = bitstampAPI;
    }

    public Observable<List<Transaction>> getTransactions() {
        return bitstampAPI
                .getTransactions()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}