package com.choco.bitcoin.data.models.transactions;

import static com.choco.bitcoin.utils.DateUtils.getDateTimeString;
import static com.choco.bitcoin.utils.StringUtils.EMPTY_STR;
import static com.choco.bitcoin.utils.StringUtils.getOkFloat;
import static com.choco.bitcoin.utils.StringUtils.getOkInt;
import static com.choco.bitcoin.utils.StringUtils.getOkLong;
import static com.choco.bitcoin.utils.StringUtils.replaceNull;

public class Transaction {
    public static final int TYPE_BUY = 0;
    public static final int TYPE_SELL = 1;

    public String amount;
    public Integer type;
    public Float price;
    public Long tid;
    public Long date;

    public String getAmount() {
        return replaceNull(amount);
    }

    public Integer getType() {
        return getOkInt(type);
    }

    public Float getPrice() {
        return getOkFloat(price);
    }

    public Long getTid() {
        return tid;
    }

    public String getDateTimeStr() {
        long timestamp = getOkLong(date);
        if (timestamp != 0) {
            return getDateTimeString(timestamp);
        } else {
            return EMPTY_STR;
        }

    }
}
