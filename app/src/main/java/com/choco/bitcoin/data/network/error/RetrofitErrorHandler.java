package com.choco.bitcoin.data.network.error;

import com.choco.bitcoin.data.network.exceptions.APIException;
import com.choco.bitcoin.data.network.exceptions.ConnectionTimeOutException;
import com.choco.bitcoin.data.network.exceptions.UnknownException;
import com.choco.bitcoin.utils.StringUtils;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

public class RetrofitErrorHandler {
    public static void handleException(Throwable e) throws
            APIException,
            UnknownException,
            ConnectionTimeOutException {

        if (e instanceof HttpException) {
            handleHttpException(e);
        } else if (e instanceof IOException) {
            throw new ConnectionTimeOutException();
        } else {
            throw new UnknownException();
        }
    }

    private static void handleHttpException(Throwable e)
            throws
            APIException,
            UnknownException {

        HttpException exception = (HttpException) e;
        Response response = exception.response();
        if (response != null) {
            APIError apiError;
            try {
                apiError = parseError(response);
            } catch (Exception e1) {
                e1.printStackTrace();
                throw new UnknownException();
            }

            if (apiError != null) {
                String errorMessage = StringUtils.replaceNull(apiError.getErrors());
                throw new APIException(errorMessage);
            }
        }
        throw new UnknownException();
    }

    private static APIError parseError(Response<?> response) throws IOException {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create().fromJson(response.errorBody().string(), APIError.class);
    }
}